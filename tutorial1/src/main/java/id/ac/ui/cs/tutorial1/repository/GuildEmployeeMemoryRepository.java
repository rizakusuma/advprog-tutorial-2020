package id.ac.ui.cs.tutorial1.repository;

import id.ac.ui.cs.tutorial1.model.GuildEmployee;

import java.util.ArrayList;
import java.util.List;

public class GuildEmployeeMemoryRepository implements GuildEmployeeRepository {
    private ArrayList<GuildEmployee> employeesList = new ArrayList<>();

    @Override
    public List<GuildEmployee> findAll() {
        return employeesList;
    }

    @Override
    public GuildEmployee findById(String id) {
        for (GuildEmployee employee : employeesList) {
            if (employee.getId().equals(id)) {
                return employee;
            }
        }
        throw new RuntimeException("Not Found");
    }


    @Override
    public void addEmployee(GuildEmployee guildEmployee) {
        employeesList.add(guildEmployee);
    }
}
