package id.ac.ui.cs.tutorial1.repository;

import id.ac.ui.cs.tutorial1.model.GuildEmployee;

import java.util.List;

public interface GuildEmployeeRepository {
    public List<GuildEmployee> findAll();
    public GuildEmployee findById(String id);
    public void addEmployee(GuildEmployee guildEmployee);
}
