1. The flow starts with a Manalith, that have 4 mana. Then there are two options to add the types of Synthesis, that are Knight and Catalyst. After that we can click the "Process Request" button to process the Manalith's request. the processRequest is caleed in the runManalith() function (that's processed within a certain thread) that causes all the different requests of synthesis to be handled concurrent and simultaneously.

2. Race Condition appeared in ManalithServiceImpl.java at line 43 and 44. the "Race" condition appeared because after the processRequest() function is called, the same function is re-called. It can also be seen in processRequest() method. This causes two threads to be made (in the runManalith method), and both threads are run simultaneously.

3. Race Condition occurs when there are multiple threads that runs concurrently and simultaneously and when there's shared data.

4. One of the solution might be to changed into thread safe queue (for example BlockingQueue).