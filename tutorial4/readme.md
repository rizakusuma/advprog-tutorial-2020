# Tutorial 4
# I Prove My Proficiency in Profiling Programs Professionally with Prometheus
The problem that The Recruiter gave you to solve earlier is very tough, but you can solve it with your abilities. You look at The Recruiter's pleased expression that you meet The Association's expectation, and indeed that is the case. The Recruiter asks if you would like to stay for a while working in Magician's Association as Magician in Internship. You glance at Guild Master. Guild Master nods in return. Looks like you don't have much of a choice. The Recruiter shows you to your room in the Tower Dormitory. There you meet your new roommate, The Assistant. That person appears to be exhilarated to finally have a roommate. Guild Master returns back to the Guild. You have dinner, and spend the night in your new room.

The Assistant tells you stories about the Magician's Association and how they also started out as a Guild member and is now a full-fledged member of the Magician's Association. You can feel their wholehearted passion of this world's magic, and can kind of relate. Over the next few days, The Assistant shows you around the association, and The Recruiter assigns you two several tasks which you complete easily. Your trust grows, and you decide to confide in The Assistant about your circumstances on how you ended up in this world, and about the magic that is very similar to your previous world. The Assistant listens to you calmly. They were shocked at first, but wholeheartedly believes in your story. They promise to help you find a way back home, if possible at all.

One sunny morning after having breakfast, you and The Assistant see [] talking with The Recruiter. You want to ask how they're related but you feel it is inappropriate to ask. Was [] also a member of the Magician's Association? The Recruiter explains that [] is looking into magic monitoring technologies, and that the Association has some available, but aren't too familiar with the concept. The term **code profiling** comes into your mind.

You heard [] mutter something about **Prometheus**, and your suddenly feel a sting in your head. You remember about Spring Boot Actuators, and several tools that are available to monitor code metrics back in your world. The Recruiter asks if you know anything, and you nod in return. You are asked to try what you know on some mantra, and [] wishes to observe. Something feels off, but you forget about it and accept the job anyway.

The Recruiter gives you some legacy magic from several years ago and asks you to try and monitor the metrics of said magic. The Assistant looks at you, full of determination. They seem to want to assist you. You smile and accept their help. *Little did you know, this job would be the last job you do together with The Assistant.*

## Monitoring Metrics
You can use Spring Boot Actuator to monitor the metrics in a Spring Boot project.

You add this dependency to this tutorial's `build.gradle`:

`compile 'org.springframework.boot:spring-boot-starter-actuator'`

and that's it, you're done! Now you have access to `localhost:8080/actuator`. Try it out.

Quest : 
- Explain what is contained in the `/actuator` endpoint.
- Try and display some data in the `/actuator/info` endpoint. Any data is fine.

## Prometheus

<<<<<<< HEAD
Prometheus is an open-source system for monitoring systems. You can get Prometheus [here]([https://prometheus.io/download/](https://prometheus.io/download/)).
=======
Prometheus is an open-source system for monitoring systems. You can get Prometheus [here](https://prometheus.io/download/).
>>>>>>> 10200973a925029366e450189f0b975b27e3e109

You can run Prometheus by running the executable (**prometheus.exe**). 
By default, it should be running on `localhost:9090`, and will be checking the metrics of Prometheus itself. You can check by visiting `localhost:9090/targets`. You should see Prometheus listed as one of the targets. You want it to show the metrics of the Spring Boot project. Before you can do this, you need to configure your Spring Boot project so that it displays a Prometheus metrics endpoint.

Now add this dependency to this tutorial's `build.gradle`:

`compile 'io.micrometer:micrometer-registry-prometheus'`

Create a new folder named `config` in the same package as the `controller` folder, and make a file named `RegistryConfig.java` inside it.

You fill `RegistryConfig.java` with the following code:
```java
package id.ac.ui.cs.advprog.tutorial4.config;

import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.micrometer.core.instrument.MeterRegistry;

@Configuration
public class RegistryConfig {

	@Bean
	MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
		return registry ->  registry.config().commonTags("app.name","tutorial4");
	}

}
```
You check `localhost:8080/actuator` again but you don't see Prometheus. That's because you'll need the application to expose all its metric endpoints. You can do that by adding this line to the **application.properties** file:
```
management.endpoints.web.exposure.include=*
```
Now try checking out the `/actuator` endpoint again, and you should see every endpoint available, and `prometheus` as one of those endpoints. 

Now that you have a Prometheus metrics endpoint, you can configure Prometheus to listen to your SpringBoot project's metrics. All Prometheus configurations are in YAML format, and are stored in the **prometheus.yml** file located in the same folder as the executable. You will have to edit this file in order for Prometheus to listen in on `localhost:8080`.

Quest :

-	Try the `/actuator/prometheus` endpoint. Look at its contents and write what you understand about it.
-	Change your **prometheus.yml** file so that it targets the Prometheus metrics endpoint at `localhost:8080`.

## Using the @Timed annotation

One of the many things you can do with SpringBoot is checking time metrics. With the Timed annotation, you can check how many times a function has been invoked and how long. You add the following code to `RegistryConfig.java`:
```java
import io.micrometer.core.aop.TimedAspect; //add this import

@Bean
TimedAspect  timedAspect(MeterRegistry  registry) {
	return  new  TimedAspect(registry);
}
```
Now you have access to the `@Timed` annotation. You can add this annotation before the function you want to test. You try adding the annotation to the customErrorPage function. Don't forget to import `io.micrometer.core.annotation.Timed`.

![goof](images/goof-annotated.PNG)

Now you have access to three new queries in your Prometheus dashboard: goof_seconds_count, goof_seconds_max, and goof_seconds_sum. 

![goofmetric](images/goof-metrics.PNG)

Quest :

- Try adding a Timed annotation to a function, check it with Prometheus, and explain what you see.

## Bonus: Grafana
Grafana is one of many tools used to display metrics, using many available preset dashboards to monitor infrastructure and architecture of applications. You can get Grafana [here](https://grafana.com/grafana/download).

By default, Grafana will run in `localhost:3000`. Once you have it set up to listen to your Prometheus data source in `localhost:9090` using a JVM Micrometer dashboard, you will have a display similar to this:

![grafana](images/grafana.jpg)

Extra Quest :

- Set-up Grafana and explain what you did

Checklist :
- [ ] Read the tutorial. Make an md file named answers.md and put all your answers to the quests above there.
- [ ] Open the `/actuator` endpoint
- [ ] Install Prometheus and run it locally at `localhost:9090`
- [ ] Configure your project to display Prometheus endpoint at `/actuator/prometheus`
- [ ] Configure your local Prometheus to listen in on your SpringBoot project
- [ ] Try and add a @Timed annotation to a function and check the metrics
- [ ] **BONUS**: Set up Grafana in your localhost, and display your Prometheus data on it.

### References:
- [Callicoder](https://www.callicoder.com/spring-boot-actuator-metrics-monitoring-dashboard-prometheus-grafana/)
- [Stackabuse](https://stackabuse.com/monitoring-spring-boot-apps-with-micrometer-prometheus-and-grafana/)
- [Ignacio Suay](http://ignaciosuay.com/how-to-visualize-spring-boot-2-metrics-with-prometheus/)
- [Prometheus in Grafana](https://grafana.com/docs/grafana/latest/features/datasources/prometheus/)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU5NTk3NzM0MiwtMTg4MDYxMzU1NCwtMT
IzODMxNTE1OCw4ODg1MDEzNjEsNTI2ODYxMTYzLC0xNzYxODE0
NzUsLTkyMzgzNzQ1LC0xNjAwNDI0ODIsLTkxNzk4Mzc0NSwtMT
gyMDA2MTA0NCwtMTY1MDM3MDczOSwtNTcyNDM5MTUzLC0yMTE4
NTU1NzU5LDk3OTYxMTg2OSwyMDc0MjUxMjA1LDE3Mzc3Mzg2ND
IsLTE2MDAzNzQ4MzEsLTIyMzcwNzM1MywyMTQ0NTcxNDIwLDMy
MTU2MTUzMl19
-->