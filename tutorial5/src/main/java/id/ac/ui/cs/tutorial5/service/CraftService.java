package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.CraftItem;

import java.util.List;

public interface CraftService {

    public CraftItem createItem(String itemName);
    public String[] getItemNames();
    public List<CraftItem> findAll();
}
