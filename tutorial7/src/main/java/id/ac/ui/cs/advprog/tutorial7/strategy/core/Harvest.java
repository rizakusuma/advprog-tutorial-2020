package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Harvest implements AttackAction {
	
	private static final String ACTION_NAME = "Harvest";
	
	public String getDescription(){
		return "An offensive death magic that can absorb targeted magician's life and magical energy";
	}
	
	public String attack(){
		return "Activate " + ACTION_NAME + ". Absorb targeted magician's life and magical energy";
	}
}