package id.ac.ui.cs.advprog.tutorial8.adapter.core;

import java.util.concurrent.ThreadLocalRandom;

public class TimeMageAspect {

    public String use() {
        return "stopping time... 7 seconds has passed";
    }
    public String name() {
        return "Time Mage Aspect";
    }

    public int magicRequirements() {
        return ThreadLocalRandom.current().nextInt(4500, 100000);
    }
}
