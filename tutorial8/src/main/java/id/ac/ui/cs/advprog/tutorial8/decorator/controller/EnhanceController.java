package id.ac.ui.cs.advprog.tutorial8.decorator.controller;

import id.ac.ui.cs.advprog.tutorial8.decorator.service.EnhanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class EnhanceController {

    @Autowired
    EnhanceService enhanceService;

    @RequestMapping(path = "/home", method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("skills", enhanceService.getAllSkills());
        return "decorator/home";
    }

    @RequestMapping(path = "/enhance-power", method = RequestMethod.POST)
    public String enhancePower(){
        enhanceService.enhancePower();
        return "redirect:/home";
    }

    @RequestMapping(path = "/enhance-manacost", method = RequestMethod.POST)
    public String enhanceManacost(){
        enhanceService.enhanceManacost();
        return "redirect:/home";
    }

    @RequestMapping(path = "/enhance-freeze", method = RequestMethod.POST)
    public String enhanceFreeze(){
        enhanceService.enhanceFreeze();
        return "redirect:/home";
    }

    @RequestMapping(path = "/enhance-lifesteal", method = RequestMethod.POST)
    public String enhanceLifesteal(){
        enhanceService.enhanceLifesteal();
        return "redirect:/home";
    }

}
