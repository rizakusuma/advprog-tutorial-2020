package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public interface Magic  {
    public String cast();
    public int getMagicCost();
    public String description();
}
