package id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

public class LifestealUpgrade extends Skill {

    Skill skill;

    public LifestealUpgrade(Skill skill) {

        this.skill= skill;
    }

    @Override
    public String getName() {
        return skill.getName();
    }

    // Adds lifesteal description to skill
    @Override
    public String getDescription() {
        //ToDo: Complete Me
        return null;
    }
}
