package id.ac.ui.cs.advprog.tutorial8.decorator.service;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.*;
import id.ac.ui.cs.advprog.tutorial8.decorator.repository.EnhanceRepository;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Service
public class EnhanceServiceImpl implements EnhanceService {

    private ArrayList<Skill> skills;
    private EnhanceRepository enhanceRepository;

    public EnhanceServiceImpl(ArrayList<Skill> skills, EnhanceRepository enhanceRepository){
        this.skills = skills;
        this.enhanceRepository = enhanceRepository;
        skillInit();
    }

    public void skillInit() {
        Skill fireball = new Fireball();
        Skill magicmissile = new MagicMissile();
        Skill thunderbolt = new Thunderbolt();
        skills.add(fireball);
        skills.add(magicmissile);
        skills.add(thunderbolt);
    }

    @Override
    public void enhancePower() {
        enhanceRepository.enhancePower(skills);
    }

    @Override
    public void enhanceManacost() {
        enhanceRepository.enhanceManacost(skills);
    }

    @Override
    public void enhanceFreeze() {
        enhanceRepository.enhanceFreeze(skills);
    }

    @Override
    public void enhanceLifesteal() {
        enhanceRepository.enhanceLifesteal(skills);
    }

    @Override
    public Iterable<Skill> getAllSkills() {
        return skills;
    }
}
