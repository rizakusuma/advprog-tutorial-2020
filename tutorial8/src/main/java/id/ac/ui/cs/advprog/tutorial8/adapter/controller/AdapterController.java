package id.ac.ui.cs.advprog.tutorial8.adapter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/adapter/magicSelection")
public class AdapterController {

    @GetMapping("/research")
    public String magicResearch() {
        return "adapter/home";
    }

}
