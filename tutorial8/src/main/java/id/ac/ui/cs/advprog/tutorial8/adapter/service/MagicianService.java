package id.ac.ui.cs.advprog.tutorial8.adapter.service;

import id.ac.ui.cs.advprog.tutorial8.adapter.core.Magician;
import id.ac.ui.cs.advprog.tutorial8.adapter.core.MagicianResponseWrapper;

import java.security.SecureRandom;
import java.util.concurrent.ThreadLocalRandom;

public class MagicianService {

    private SecureRandom secureRandom = new SecureRandom();
    private MagicService magicService = new MagicService();
    private Magician magician;


    public MagicianResponseWrapper findMagician() {
        MagicianResponseWrapper magicianResponseWrapper = new MagicianResponseWrapper();
        Magician magician =getMagicianIfNotExists();
        magicianResponseWrapper.setLearn(magician.learnMagic());
        magicianResponseWrapper.setAttack(magician.attack());
        return magicianResponseWrapper;
    }

    public Magician getMagicianIfNotExists() {
        if (magician==null) {
            this.magician =createNew();
        }
        return magician;
    }

    public void learnMagic(String magicName) {
        this.magician.learnMagic(this.magicService.getMagicInstance(magicName));
    }

    public void resetMagician() {
        this.magician = createNew();
    }

    public Magician createNew() {
        return new Magician("Random Magician", ThreadLocalRandom.current().nextInt(2500, 100000));
    }


}
