package id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

import java.util.Random;

public class ManacostUpgrade extends Skill {

    Skill skill;

    public ManacostUpgrade(Skill skill) {

        this.skill= skill;
    }

    @Override
    public String getName() {
        return skill.getName();
    }

    // Reduces skill manacost
    @Override
    public int getSkillManacost() {
        //ToDo: Complete me
        return 0;
    }

    @Override
    public String getDescription() {
        return this.skill.getDescription() + " + manacost reduction";
    }
}
